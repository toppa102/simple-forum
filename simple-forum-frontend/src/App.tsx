import { Component, onMount } from 'solid-js';

import { Routes, Route, A, useNavigate } from '@solidjs/router';
import Login from './pages/Login';
import Register from './pages/Register';

import AddPost from './pages/AddPost';
import Post from './pages/Post';
import Posts from './pages/Posts';

const App: Component = () => {

  const navigate = useNavigate();

  onMount(() => {
    if (!window.localStorage.getItem('token')) {
      navigate('/login');
    }
  });

  return (
    <div class='bg-gray-900 min-h-screen text-white p-5'>
      <div class='m-auto w-2/5'>
        <Routes>
          <Route path="/posts/:id" component={Post}></Route>
          <Route path="/login" component={Login}></Route>
          <Route path="/register" component={Register}></Route>
          <Route path="/" component={Posts}></Route>
        </Routes>
      </div>
    </div>
  );
};

export default App;
