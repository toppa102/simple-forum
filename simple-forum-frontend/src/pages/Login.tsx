import { A, useNavigate } from '@solidjs/router';
import type { Component } from 'solid-js';
import { createStore } from 'solid-js/store';

const API_URL = 'http://localhost:5078'

const Login: Component = () => {
    const navigate = useNavigate();
    const [fields, setFields] = createStore({ username: "", password: "" });

    const loginSubmit = async (e: SubmitEvent) => {
        e.preventDefault();
        const res = await fetch(`${API_URL}/api/token`,
            {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(fields)
            });

        if (res.ok) {
            window.localStorage.setItem('token', await res.text());
            navigate("/");
        } else {
            alert("Wrong username or password");
        }
    }

    return (
        <div>
            <form onSubmit={loginSubmit} class='space-y-2 w-44 m-auto top-20'>
                <div>
                    <input
                        class='p-0.5 bg-gray-700 w-44'
                        name="username"
                        type="username"
                        placeholder="Username"
                        onInput={(e) => setFields("username", e.target.value)}
                        required
                    />
                </div>
                <div>
                    <input
                        class='p-0.5 bg-gray-700 w-44'
                        type="password"
                        name="password"
                        placeholder="Password"
                        oninput={(e) => setFields("password", e.target.value)}
                    />
                </div>
                <button type="submit">Login</button>

                <A class='float-right' href="/register">Register</A>
            </form>
        </div>
    );
};

export default Login;