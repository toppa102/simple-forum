import { useNavigate, useParams } from '@solidjs/router';
import { Component, For, Match, Show, Suspense, Switch, createResource, createSignal, lazy, onMount } from 'solid-js';
import { createStore } from 'solid-js/store';
import AddReply from './AddReply';

export const getTimeString = (timeString: string) => {
    let lastUpdated = Date.parse(timeString);
    let now = new Date();
    let seconds = (now.getTime() - lastUpdated) / 1000;
    if (Math.floor(seconds / (60 * 60 * 24)) > 0) {
        return (seconds / 60 / 60 / 24).toFixed(0).toString() + " days ago";
    }
    if (Math.floor(seconds / (60 * 60))) {
        return (seconds / 60 / 60).toFixed(0).toString() + " hours ago";
    }
    if (Math.floor(seconds / 60)) {
        return (seconds / 60).toFixed(0).toString() + " minutes ago";
    }

    return seconds.toFixed(0).toString() + " seconds ago";
}

const API_URL = 'http://localhost:5078'

const getPost = async (id: string) => {
    let res = await fetch(`${API_URL}/api/posts/${id}`,
        {
            method: "GET",
            headers: { "Authorization": `Bearer ${window.localStorage.getItem('token')}` }
        })

    if (!res.ok)
        return Promise.reject("Post not found!");

    return res.json();
}

const getReplies = async (id: string) => {
    let res = await fetch(`${API_URL}/api/posts/${id}/replies`,
        {
            method: "GET",
            headers: { "Authorization": `Bearer ${window.localStorage.getItem('token')}` }
        })

    if (!res.ok)
        return Promise.reject("Post not found!");

    return res.json();
}


const Post: Component = () => {
    const params = useParams();
    const [post] = createResource(params.id, getPost);
    const [replies, { mutate, refetch }] = createResource(params.id, getReplies);

    const [addReply, setAddReply] = createSignal(false);
    return (
        <div class='space-y-2 '>
            <button class='bg-blue-500 text-xl rounded-full p-2' onclick={() => setAddReply(!addReply())}>Reply</button>
            <Show when={addReply()}>
                <AddReply refetch={refetch} id={params.id}></AddReply>
            </Show>
            <Show when={post.state === 'ready'}>
                <div class='bg-gray-800 p-5 rounded-lg p-y-5'>
                    <h2 class='text-xl font-bold'>{post().topic}</h2>
                    <p>{post().content}</p>
                    <p class='text-right'>{getTimeString(post().lastUpdated)}</p>
                </div>
            </Show>

            <For each={replies()} >{(reply, i) =>
                <div class='bg-gray-800 p-5 rounded-lg '>
                    <p>{reply.content}</p>
                    <p class='text-right'>{getTimeString(reply.createTime)}</p>
                </div>
            }</For>

        </div >
    );
};

export default Post;