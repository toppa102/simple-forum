import { A, useNavigate } from '@solidjs/router';
import { Component, For, Match, Suspense, Switch, createResource, createSignal, onMount } from 'solid-js';
import { createStore } from 'solid-js/store';

import { getTimeString } from './Post';
import AddPost from './AddPost';

const API_URL = 'http://localhost:5078'

const getPosts = async () =>
    (await fetch(`${API_URL}/api/posts`,
        {
            method: "GET",
            headers: { "Authorization": `Bearer ${window.localStorage.getItem('token')}` }
        })).json()



const Posts: Component = () => {

    const [posts, { mutate, refetch }] = createResource(getPosts);
    return (
        <div class='space-y-2'>
            <AddPost refetch={refetch}></AddPost>
            <Switch>
                <Match when={posts.state === 'ready'}>
                    <For each={posts().reverse()} >{(post, i) =>
                        <A href={`/posts/${post.id}`} style="display: block;">
                            <div class='bg-gray-800 p-5 rounded-lg '>
                                <h2 class='text-xl font-bold'>{post.topic}</h2>
                                <p>{post.content}</p>
                                <p class='text-right'>{getTimeString(post.lastUpdated)}</p>
                            </div>
                        </A>
                    }</For>
                </Match>
            </Switch>
        </div>
    );
};

export default Posts;