import { useNavigate } from '@solidjs/router';
import type { Component } from 'solid-js';
import { createStore } from 'solid-js/store';

const API_URL = 'http://localhost:5078'

const Register: Component = () => {
    const navigate = useNavigate();
    const [fields, setFields] = createStore({ username: "", email: "", password: "" });

    const loginSubmit = async (e: SubmitEvent) => {
        e.preventDefault();
        const res = await fetch(`${API_URL}/api/register`,
            {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(fields)
            });


        if (res.ok) {
            navigate("/login");
        } else {
            alert("Error while registering!");
        }
    }

    return (
        <div>
            <form onSubmit={loginSubmit} class='space-y-2 w-44 m-auto top-20'>
                <div>
                    <input
                        class='p-0.5 bg-gray-700'
                        name="username"
                        type="username"
                        placeholder="Username"
                        onInput={(e) => setFields("username", e.target.value)}
                        required
                    />
                </div>
                <div>
                    <input
                        class='p-0.5 bg-gray-700'
                        name="email"
                        type="email"
                        placeholder="Email"
                        onInput={(e) => setFields("email", e.target.value)}
                        required
                    />
                </div>
                <div>
                    <input
                        class='p-0.5 bg-gray-700'
                        type="password"
                        name="password"
                        placeholder="Password"
                        oninput={(e) => setFields("password", e.target.value)}
                    />
                </div>
                <button type="submit">Register</button>
            </form>
        </div>
    );
};

export default Register;