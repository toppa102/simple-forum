import { useNavigate } from '@solidjs/router';
import type { Component } from 'solid-js';
import { createStore } from 'solid-js/store';

const API_URL = 'http://localhost:5078'


const AddPost: Component<{ refetch: any }> = (props) => {
    const navigate = useNavigate();
    const [fields, setFields] = createStore({ topic: "", content: "" });

    const loginSubmit = async (e: SubmitEvent) => {
        e.preventDefault();
        const res = await fetch(`${API_URL}/api/posts`,
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${window.localStorage.getItem('token')}`
                },
                body: JSON.stringify(fields)
            });
        if (res.ok) {
            props.refetch();
        }
    }

    return (
        <div>
            <form onSubmit={loginSubmit} class='space-y-2 '>
                <div>
                    <input
                        class='p-0.5 bg-gray-700 w-full'
                        name="topic"
                        placeholder="Topic"
                        onInput={(e) => setFields("topic", e.target.value)}
                        required
                    />
                </div>
                <div>
                    <textarea
                        class='p-0.5 bg-gray-700 h-32 w-full'
                        name="content"
                        placeholder="Content"
                        oninput={(e) => setFields("content", e.target.value)}
                        required
                    />
                </div>
                <button type="submit" class='bg-blue-500 text-xl rounded-full p-2'>Send Post</button>
            </form>
        </div>
    );
};

export default AddPost;