using Microsoft.EntityFrameworkCore;

public class ForumContext : DbContext
{
    public DbSet<User> users { get; set; }
    public DbSet<Post> posts { get; set; }
    public DbSet<Reply> replies { get; set; }

    public IConfiguration _config;

    public ForumContext(IConfiguration config)
    {
        _config = config;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql(_config.GetConnectionString("ForumContext"));
}