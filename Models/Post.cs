using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

public class Post
{
    public int id { get; set; }

    public string topic { get; set; } = null!;

    public string content { get; set; } = null!;
    public DateTime createTime { get; set; }
    public DateTime lastUpdated { get; set; }

    public int userId { get; set; }

    [JsonIgnore]
    public User user { get; set; }

    [JsonIgnore]
    public List<Reply> replies { get; set; } = new();
}