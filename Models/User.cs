using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

public class User
{
    public int id { get; set; }
    [Required]
    public string userName { get; set; } = null!;
    [Required]
    public string password { get; set; } = null!;
    [EmailAddress]
    public string? email { get; set; }


    [JsonIgnore]
    public List<Post> posts { get; } = new();

    [JsonIgnore]
    public List<Reply> replies { get; } = new();
}