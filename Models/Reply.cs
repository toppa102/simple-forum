using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

public class Reply
{
    public int id { get; set; }
    public string content { get; set; } = null!;
    public DateTime createTime { get; set; }

    public int userId { get; set; }

    // Avoid object cycles
    [JsonIgnore]
    public User user { get; set; }

    public int postId { get; set; }

    // Avoid object cycles
    [JsonIgnore]
    public Post post { get; set; }
}