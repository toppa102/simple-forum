using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Npgsql;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;

public class PostData
{
    public string topic { get; set; } = null!;

    public string content { get; set; } = null!;
}

public class ReplyData
{
    public string content { get; set; } = null!;
}

[ApiController, Authorize]
[Route("/api")]
public class PostController : Controller
{

    private readonly ILogger<PostController> _logger;

    public IConfiguration _config;
    private ForumContext _context;

    public PostController(IConfiguration config, ILogger<PostController> logger, ForumContext context)
    {
        _config = config;
        _logger = logger;
        _context = context;
    }

    [HttpGet]
    [Route("posts")]
    public IAsyncEnumerable<Post> Posts()
    {
        var posts = _context.posts.AsAsyncEnumerable();

        return posts;
    }

    [HttpGet]
    [Route("posts/{id}")]
    public async Task<IActionResult> GetPost(int id)
    {
        try
        {
            var post = await _context.posts.Where(p => p.id == id).FirstAsync();
            return Ok(post);
        }
        catch (Exception e)
        {
            return NotFound();
        }
    }

    [HttpPost]
    [Route("posts")]
    public async Task<IActionResult> insertPost([FromBody] PostData _post)
    {
        var identity = HttpContext.User.Identity as ClaimsIdentity;
        if (identity != null)
        {
            var userClaims = identity.Claims;
            var userId = int.Parse(userClaims.FirstOrDefault(x => x.Type == "id").Value);

            var user = await _context.users
                .Include(user => user.posts)
                .Where(u => u.id == userId).FirstAsync();

            var post = new Post
            {
                topic = _post.topic,
                content = _post.content,
                createTime = DateTime.UtcNow,
                lastUpdated = DateTime.UtcNow,
            };

            user.posts.Add(post);

            await _context.SaveChangesAsync();
            return Ok();
        }
        return BadRequest("Error while posting!");

    }

    [HttpGet]
    [Route("posts/{id}/replies")]
    public async Task<IEnumerable<Reply>> getReplies(int id)
    {
        var post = await _context.posts
            .Include(p => p.replies)
            .Where(p => p.id == id).FirstAsync();

        return post.replies.ToList();
    }

    [HttpPost]
    [Route("posts/{id}/replies")]
    public async Task<IActionResult> insertReply(int id, [FromBody] string content)
    {
        var post = await _context.posts.Where(p => p.id == id).FirstAsync();
        var identity = HttpContext.User.Identity as ClaimsIdentity;
        if (identity != null)
        {
            var userClaims = identity.Claims;
            var userId = int.Parse(userClaims.FirstOrDefault(x => x.Type == "id").Value);
            var user = await _context.users.Where(u => u.id == userId).FirstAsync();

            var reply = new Reply
            {
                content = content,
                createTime = DateTime.UtcNow,
                userId = userId,
            };

            post.replies.Add(reply);
            await _context.SaveChangesAsync();

            return Ok();
        }

        return BadRequest("Not logged in?");
    }
}
