using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Microsoft.IdentityModel.Tokens;
using Npgsql;
using Microsoft.EntityFrameworkCore;

public class UserData
{
    [Required]
    public string userName { get; set; } = null!;
    [Required]
    public string password { get; set; } = null!;
    [EmailAddress]
    public string? email { get; set; }
}

[ApiController]
[Route("/api")]
public class UserController : ControllerBase
{
    private readonly ILogger<UserController> _logger;

    public IConfiguration _config;
    private ForumContext _context;

    public UserController(IConfiguration config, ILogger<UserController> logger, ForumContext context)
    {
        _config = config;
        _logger = logger;
        _context = context;
    }


    [HttpPost]
    [Route("register")]
    public async Task<IActionResult> Register([Bind()] UserData _user)
    {
        if (_user.email == null)
        {
            return BadRequest("No email provided...");
        }

        var user = new User
        {
            userName = _user.userName,
            password = _user.password,
            email = _user.email
        };

        await _context.users.AddAsync(user);

        await _context.SaveChangesAsync();
        return Ok();
    }


    [HttpPost]
    [Route("token")]
    public async Task<IActionResult> Login([FromBody] UserData _user)
    {
        var user = await _context.users.Where(u => u.userName == _user.userName).FirstAsync();
        if (user.password == _user.password)
        {
            Claim[] claims =
            {
                        new Claim(JwtRegisteredClaimNames.Sub, "TokenForTheApiWithAuth"),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                        new Claim("id", user.id.ToString()),
                        new Claim("username", user.userName),
                        new Claim("email", user.email!)
                    };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("0123456789012345"));
            var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                "apiWithAuthBackend",
                "apiWithAuthBackend",
                claims,
                expires: DateTime.UtcNow.AddDays(7),
                signingCredentials: signIn);

            return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        }


        return BadRequest("Invalid credentials");
    }
}
