# Simple forum

Simple forum with register, login, posting, replying functionality with JWT
authentication (not tested/secure). Frontend is
[SolidJS](https://www.solidjs.com/) (very similiar api to React) and backend is
ASP.NET Core and EF Core.

## Technical stuff

This was originally written as my SQL courses final project which is why I was
not using Entity Framework at first. But I changed it to EF Core.

## Example usage

### Register

![](docs/example1.png)

### Login

![](docs/example2.png)

### Making a post

![](docs/example3.png)

### Replying to a post

![](docs/example4.png)
